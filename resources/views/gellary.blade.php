<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="author" content="">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<title>Мэдээ Мэдээлэл</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" href="css/style.css">
<link href="https://fonts.googleapis.com/css?family=Lora|Merriweather:300,400" rel="stylesheet">

</head>
<body>

<div class="preloader">
     <div class="sk-spinner sk-spinner-wordpress">
          <span class="sk-inner-circle"></span>
     </div>
</div>

<div class="navbar navbar-default navbar-static-top" role="navigation">
     <div class="container">

          <div class="navbar-header">
               <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
               </button>
               <a href="index" class="navbar-brand">NEWWOLRD</a>
          </div>
          <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
                    <li><a href="index">Нүүр</a></li>
                    <li><a href="about">Бидний тухай</a></li>
                    <li class="active"><a href="gallery">Папараци</a></li>
                    <li><a href="contact">Хүсэлт илгээх</a></li>
               </ul>
          </div>

  </div>
</div>

<section id="home" class="main-gallery parallax-section">
     <div class="overlay"></div>
     <div class="container">
          <div class="row">

               <div class="col-md-12 col-sm-12">
                    <h1>Мэдээ Мэдээлэл</h1>
               </div>

          </div>
     </div>
</section>

<section id="gallery">
     <div class="container">
          <div class="row">

               <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <h2>Beautiful Images with Magnific Popup..</h2>
                    <p>Aliquam blandit velit nisi, sed fringilla felis lacinia sed. Integer vitae porta felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus non tristique lacus. Suspendisse ut tortor vitae risus lacinia tristique. Aliquam sed consectetur libero.</p>
                    <p>Morbi tellus dolor, porta dignissim enim sit amet, dapibus sagittis erat. In blandit elit sit amet dui aliquet congue nec vel quam. Integer id tristique libero.</p>
                    <span></span>
                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image1.jpg" class="image-popup">
                                   <img src="images/gallery-image1.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image2.jpg" class="image-popup">
                                   <img src="images/gallery-image2.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image3.jpg" class="image-popup">
                                   <img src="images/gallery-image3.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image4.jpg" class="image-popup">
                                   <img src="images/gallery-image4.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image5.jpg" class="image-popup">
                                   <img src="images/gallery-image5.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="gallery-thumb">
                              <a href="images/gallery-image6.jpg" class="image-popup">
                                   <img src="images/gallery-image6.jpg" class="img-responsive" alt="Gallery Image">
                              </a>
                         </div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                         <p>Aliquam blandit velit nisi, sed fringilla felis lacinia sed. Integer vitae porta felis. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Phasellus non tristique lacus.</p>
                    </div>
               </div>

          </div>
     </div>
</section>

<footer>
     <div class="container">
          <div class="row">

               <div class="col-md-5 col-md-offset-1 col-sm-6">
                    <h3>NewWorld</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    <div class="footer-copyright">
                         <p>Copyright &copy; 2019 Your Company - Design: Tooplate</p>
                    </div>
               </div>

               <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h3>Лавлах</h3>
                    <p><i class="fa fa-globe"></i> Монгол улс, Улаанбаатар хот, Сүхбаатар дүүргийн 12-р хороо Доржоогийн гудамж</p>
                    <p><i class="fa fa-phone"></i> +97695546190</p>
                    <p><i class="fa fa-save"></i> doogiitsogtoo08@gmail.com</p>
               </div>

               <div class="clearfix col-md-12 col-sm-12">
                    <hr>
               </div>
          </div>
     </div>
</footer>

<a href="#back-top" class="go-top"><i class="fa fa-angle-up"></i></a>

</body>
</html>