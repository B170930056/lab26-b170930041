
 @include('/includes/header')
<body>

<div class="preloader">
     <div class="sk-spinner sk-spinner-wordpress">
          <span class="sk-inner-circle"></span>
     </div>
</div>

<div class="navbar navbar-default navbar-static-top" role="navigation">
     <div class="container">

          <div class="navbar-header">
               <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
                    <span class="icon icon-bar"></span>
               </button>
               <a href="index" class="navbar-brand">NewWorld</a>
          </div>
          <div class="collapse navbar-collapse">
               <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="/index">Нүүр</a></li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Улс төр<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="/about">Мэдээ</a></li>
                              <li><a href="/about">Улсын их хурал</a></li>
                              <li><a href="/about">Ерөнхийлөгч</a></li>
                              <li><a href="/about">Засгийн газар</a></li>
                              <li><a href="/about">Намууд</a></li>
                         </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Эдийн засаг<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="/about">Мэдээ</a></li>
                              <li><a href="/about">Санхүүгийн зах зээл</a></li>
                              <li><a href="/about">Уул уурхай</a></li>
                              <li><a href="/about">Үнэ ханш</a></li>
                              <li><a href="/about">Бизнес</a></li>
                         </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Нийгэм<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="#">Мэдээ</a></li>
                              <li><a href="/about">Эрүүл мэнд</a></li>
                              <li><a href="/about">Боловсрол</a></li>
                              <li><a href="/about">Байгаль орчин</a></li>
                              <li><a href="/about">Хууль, шүүх</a></li>
                              <li><a href="/about">Сурвалжлага</a></li>
                         </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Дэлхий<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="/about">Мэдээ</a></li>
                              <li><a href="/about">Халуун сэдэв</a></li>
                              <li><a href="/about">Хөрш орнууд</a></li>
                              <li><a href="/about">Бүс нутаг</a></li>
                              <li><a href="/about">НҮБ</a></li>
                         </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Спорт<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="/about">Мэдээ</a></li>
                              <li><a href="/about">Спорт тоглоом</a></li>
                              <li><a href="/about">Бөхийн төрлүүд</a></li>
                              <li><a href="/about">Үндэсний спорт</a></li>
                              <li><a href="/about">Олимп</a></li>
                         </ul>
                    </li>
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="/about">Энтертайнмент<span class="caret"></span></a>
                         <ul class="dropdown-menu">
                              <li><a href="/about">Мэдээ</a></li>
                              <li><a href="/about">Ярилцлага</a></li>
                              <li><a href="/about">Нийтлэл</a></li>
                              <li><a href="/about">Технологи</a></li>
                              <li><a href="/about">Фото</a></li>
                         </ul>
                    </li>
                    <li><a href="/gellary">Папараци</a></li>
                    <li><a href="/about">Бидний тухай</a></li>
                    <li><a href="/contact">Хүсэлт илгээх</a></li>
               </ul>
          </div>

  </div>
</div>

<section id="home" class="main-home parallax-section">
     <div class="overlay"></div>
     <div id="particles-js"></div>
     <div class="container">
          <div class="row">

               <div class="col-md-12 col-sm-12">
                    <h1>Мэдээ мэдээллийн сайтад тавтай морил</h1>
                    <a href="#blog" class="smoothScroll btn btn-default">Мэдээ үзэх</a>
               </div>

          </div>
     </div>
</section>

<section id="blog">
     <div class="container">
          <div class="row">

               <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <div class="blog-post-thumb">
                         <div class="blog-post-image">
                              <a href="single-post.html">
                                   <img src="{{asset('/images/1.jpg')}}" class="img-responsive" alt="Blog Image">
                              </a>
                         </div>
                         <div class="blog-post-title">
                              <h3><a href="single-post.html">Sun Bear</a></h3>
                         </div>
                         <div class="blog-post-format">
                              <span><a href="#"><img src="images/author-image1.jpg" class="img-responsive img-circle"> Jen Lopez</a></span>
                              <span><i class="fa fa-date"></i> July 22, 2017</span>
                              <span><a href="#"><i class="fa fa-comment-o"></i> 35 Comments</a></span>
                         </div>
                         <div class="blog-post-des">
                              <p>Зүүн өмнөд азийн нутагт амьдардаг. 1,2м дундаж өндөртэй, гэр бvлээрээ амьдардаг, их тачирхан үстэй 5см тул заримдаа тэрнийг нохой баавгай гэх нь ч бий. Нас бие гvйцсэн баавгай 65 кг жинтэй. Эр нь эмээсээ арай том.</p>
                              <a href="single-post.html" class="btn btn-default">Үргэлжлүүлж унших</a>
                         </div>
                    </div>

                    <div class="blog-post-thumb">
                         <div class="blog-post-image">
                              <a href="single-post.html">
                                   <img src="images/2.jpg" class="img-responsive" alt="Blog Image">
                              </a>
                         </div>
                         <div class="blog-post-title">
                              <h3><a href="single-post.html">Red Panda</a></h3>
                         </div>
                         <div class="blog-post-format">
                              <span><a href="#"><img src="images/author-image2.jpg" class="img-responsive img-circle"> Leo Dennis</a></span>
                              <span><i class="fa fa-date"></i> June 10, 2017</span>
                              <span><a href="#"><i class="fa fa-comment-o"></i> 48 Comments</a></span>
                         </div>
                         <div class="blog-post-des">
                              <p>Гэрэлтэх гэсэн латин vгнээс гаралтай улаан панда маань євсєн тэжээлтэй бєгєєд 55см хvртэл єсдєг. Гималайн оргил орчмоор болон  Хятадын ємнєд нутгаар нутагладаг. Панда гэдэг нь анх Непалчуудын “ponya” гэсэн нэрнээс гаралтай ажээ. Энэ нь хулс, ургамал иддэг амьтан гэсэн утгатай юм байна.</p>
                              <a href="single-post.html" class="btn btn-default">Үргэлжлүүлж унших</a>
                         </div>
                    </div>

                    <div class="blog-post-thumb">
                         <div class="blog-post-video">
                              <div class="embed-responsive embed-responsive-16by9">
                                   <iframe class="embed-responsive-item" src="https://www.youtube.com/watch?v=OTp8W251aiQ" allowfullscreen></iframe>
                              </div>
                         </div>
                         <div class="blog-post-title">
                              <h3><a href="single-post.html">Sloth</a></h3>
                         </div>
                         <div class="blog-post-format">
                              <span><a href="#"><img src="images/author-image1.jpg" class="img-responsive img-circle"> Jen Lopez</a></span>
                              <span><i class="fa fa-date"></i> May 30, 2017</span>
                              <span><a href="#"><i class="fa fa-comment-o"></i> 63 Comments</a></span>
                         </div>
                         <div class="blog-post-des">
                              <p>Америк тивийн тєв хэсэг, ємнєд хэсгээр нутагладаг. Биеийн температур нь 30-34 градус цельс бєгєєд идсэн хоол тэжээлээ маш удаан хадгалж их илчлэг гаргахыг хичээдэг.</p>
                              <a href="single-post.html" class="btn btn-default">Үргэлжлүүлж унших</a>
                         </div>
                    </div>

                    <div class="blog-post-thumb">
                         <div class="blog-post-image">
                              <a href="single-post.html">
                                   <img src="images/3.jpg" class="img-responsive" alt="Blog Image">
                              </a>
                         </div>
                         <div class="blog-post-title">
                              <h3><a href="single-post.html">Axolotl</a></h3>
                         </div>
                         <div class="blog-post-format">
                              <span><a href="#"><img src="images/author-image2.jpg" class="img-responsive img-circle"> Leo Dennis</a></span>
                              <span><i class="fa fa-date"></i> April 18, 2017</span>
                              <span><a href="#"><i class="fa fa-comment-o"></i> 124 Comments</a></span>
                         </div>
                         <div class="blog-post-des">
                              <p>Аjolote ч гэгддэг. Мексикээр Ambystoma гэх энэ хачин амьтан Мексикт анх олдсон. Мексик хотын доогуур урсах голд амьдардаг ба шинжлэх ухааны эрдэмтдийн хамгийн их сонирхлыг татдаг амьтан. Тэр тасарсан эд эрхтнээ єєрєє нєхєн тєлжvvлэх чадвартай гэнэ. АНУ, Австрали, Япончууд тvvнийг Wooper Rooper гэдэг бєгєєд их худалдан авдаг.</p>
                              <a href="single-post.html" class="btn btn-default">Үргэлжлүүлж унших</a>
                         </div>
                    </div>

                    <div class="blog-post-thumb">
                         <div class="blog-post-image">
                              <a href="single-post.html">
                                   <img src="images/4.jpg" class="img-responsive" alt="Blog Image">
                              </a>
                         </div>
                         <div class="blog-post-title">
                              <h3><a href="single-post.html">Blobfish</a></h3>
                         </div>
                         <div class="blog-post-format">
                              <span><a href="#"><img src="images/author-image1.jpg" class="img-responsive img-circle"> Jen Lopez</a></span>
                              <span><i class="fa fa-date"></i> March 12, 2017</span>
                              <span><a href="#"><i class="fa fa-comment-o"></i> 256 Comments</a></span>
                         </div>
                         <div class="blog-post-des">
                              <p>(Psychrolutes marcidus) далайн гvнд амьдардаг. Андуурч харвал хvнтэй адилхан харагдахаар. Биеийн жин багатай нь тvvнийг булчингаараа биш далайн усны энерцээр явж идэш тэжээлээ олоход нь тусалдаг.</p>
                              <a href="single-post.html" class="btn btn-default">Үргэлжлүүлж унших</a>
                         </div>
                    </div>
               </div>

          </div>
     </div>
</section>

 @include('/includes/foter')

</body>
</html>
