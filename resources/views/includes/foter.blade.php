
<footer>
     <div class="container">
          <div class="row">

               <div class="col-md-5 col-md-offset-1 col-sm-6">
                    <h3>NewWorld</h3>
                    <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    <div class="footer-copyright">
                         <p>Copyright &copy; 2019 Your Company - Design: Tooplate</p>
                    </div>
               </div>

               <div class="col-md-4 col-md-offset-1 col-sm-6">
                    <h3>Лавлах</h3>
                    <p><i class="fa fa-globe"></i> Монгол улс, Улаанбаатар хот, Сүхбаатар дүүргийн 12-р хороо Доржоогийн гудамж</p>
                    <p><i class="fa fa-phone"></i> +97695546190</p>
                    <p><i class="fa fa-save"></i> doogiitsogtoo08@gmail.com</p>
               </div>

               <div class="clearfix col-md-12 col-sm-12">
                    <hr>
               </div>

               <div class="col-md-12 col-sm-12">
                    <ul class="social-icon">
                         <li><a href="#" class="fa fa-facebook"></a></li>
                         <li><a href="#" class="fa fa-twitter"></a></li>
                         <li><a href="#" class="fa fa-google-plus"></a></li>
                         <li><a href="#" class="fa fa-dribbble"></a></li>
                         <li><a href="#" class="fa fa-linkedin"></a></li>
                    </ul>
               </div>
               
          </div>
     </div>
</footer>

<a href="#back-top" class="go-top"><i class="fa fa-angle-up"></i></a>


<script src="{{asset('/js/jquery.js')}}"></script>
<script src="{{asset('/js/bootstrap.min.js')}}"></script>
<script src="{{asset('/js/particles.min.js')}}"></script>
<script src="{{asset('/js/app.js')}}"></script>
<script src="{{asset('/js/jquery.parallax.js')}}"></script>
<script src="{{asset('/js/smoothscroll.js')}}"></script>
<script src="{{asset('/js/custom.js')}}"></script>

